<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A45652">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Renatus Harris, organ-maker, his challenge to Mr. Bernard Smith, organ-maker</title>
    <author>Harris, Renatus, 1640?-1715?</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A45652 of text R7967 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing H867A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A45652</idno>
    <idno type="STC">Wing H867A</idno>
    <idno type="STC">ESTC R7967</idno>
    <idno type="EEBO-CITATION">12193989</idno>
    <idno type="OCLC">ocm 12193989</idno>
    <idno type="VID">55961</idno>
    <idno type="PROQUESTGOID">2264192375</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A45652)</note>
    <note>Transcribed from: (Early English Books Online ; image set 55961)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 101:11)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Renatus Harris, organ-maker, his challenge to Mr. Bernard Smith, organ-maker</title>
      <author>Harris, Renatus, 1640?-1715?</author>
      <author>Smith, Bernard, 1630?-1708.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London? :</pubPlace>
      <date>168-?]</date>
     </publicationStmt>
     <notesStmt>
      <note>Place and date of publication from Wing.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Organ builders -- England.</term>
     <term>Broadsides -- England -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>Renatus Harris, organ-maker, his challenge to Mr. Bernard Smith, organ-maker.</ep:title>
    <ep:author>Harris, Renatus, </ep:author>
    <ep:publicationYear>1688</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>472</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-11</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-01</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-02</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2008-02</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A45652-t">
  <body xml:id="A45652-e0">
   <div type="text" xml:id="A45652-e10">
    <pb facs="tcp:55961:1" rend="simple:additions" xml:id="A45652-001-a"/>
    <head xml:id="A45652-e20">
     <w lemma="Renatus" pos="nn1" xml:id="A45652-001-a-0010">Renatus</w>
     <w lemma="Harris" pos="nn1" xml:id="A45652-001-a-0020">Harris</w>
     <pc xml:id="A45652-001-a-0030">,</pc>
     <hi xml:id="A45652-e30">
      <w lemma="organ-maker" pos="n1" xml:id="A45652-001-a-0040">Organ-maker</w>
      <pc xml:id="A45652-001-a-0050">,</pc>
      <w lemma="his" pos="po" xml:id="A45652-001-a-0060">his</w>
      <w lemma="challenge" pos="n1" xml:id="A45652-001-a-0070">Challenge</w>
      <w lemma="to" pos="acp" xml:id="A45652-001-a-0080">to</w>
      <w lemma="mr." pos="ab" xml:id="A45652-001-a-0090">Mr.</w>
     </hi>
     <w lemma="Bernard" pos="nn1" xml:id="A45652-001-a-0100">Bernard</w>
     <w lemma="Smith" pos="nn1" xml:id="A45652-001-a-0110">Smith</w>
     <pc xml:id="A45652-001-a-0120">,</pc>
     <w lemma="organ-maker" pos="n1" rend="hi" xml:id="A45652-001-a-0130">Organ-maker</w>
     <pc unit="sentence" xml:id="A45652-001-a-0140">.</pc>
    </head>
    <p xml:id="A45652-e50">
     <w lemma="it" pos="pn" xml:id="A45652-001-a-0150">IT</w>
     <w lemma="may" pos="vmd" xml:id="A45652-001-a-0160">might</w>
     <w lemma="seem" pos="vvi" xml:id="A45652-001-a-0170">seem</w>
     <w lemma="odd" pos="j" xml:id="A45652-001-a-0180">odd</w>
     <w lemma="that" pos="cs" xml:id="A45652-001-a-0190">that</w>
     <w lemma="i" pos="pns" xml:id="A45652-001-a-0200">I</w>
     <w lemma="shall" pos="vmd" xml:id="A45652-001-a-0210">should</w>
     <w lemma="make" pos="vvi" xml:id="A45652-001-a-0220">make</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-0230">the</w>
     <w lemma="public" pos="j" reg="public" xml:id="A45652-001-a-0240">publick</w>
     <w lemma="defiance" pos="n1" xml:id="A45652-001-a-0250">defiance</w>
     <w lemma="to" pos="acp" xml:id="A45652-001-a-0260">to</w>
     <w lemma="mr." pos="ab" xml:id="A45652-001-a-0270">Mr.</w>
     <w lemma="Smith" pos="nn1" rend="hi" xml:id="A45652-001-a-0280">Smith</w>
     <pc xml:id="A45652-001-a-0290">,</pc>
     <w lemma="which" pos="crq" xml:id="A45652-001-a-0300">which</w>
     <w lemma="hereafter" pos="av" xml:id="A45652-001-a-0310">hereafter</w>
     <w lemma="follow" pos="vvz" xml:id="A45652-001-a-0320">follows</w>
     <pc xml:id="A45652-001-a-0330">,</pc>
     <w lemma="if" pos="cs" xml:id="A45652-001-a-0340">if</w>
     <w lemma="i" pos="pns" xml:id="A45652-001-a-0350">I</w>
     <w lemma="do" pos="vvd" xml:id="A45652-001-a-0360">did</w>
     <w lemma="not" pos="xx" xml:id="A45652-001-a-0370">not</w>
     <w lemma="inform" pos="vvi" xml:id="A45652-001-a-0380">inform</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-0390">the</w>
     <w lemma="world" pos="n1" xml:id="A45652-001-a-0400">World</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-0410">of</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-0420">the</w>
     <w lemma="occasion" pos="n1" xml:id="A45652-001-a-0430">occasion</w>
     <pc unit="sentence" xml:id="A45652-001-a-0440">.</pc>
     <w lemma="the" pos="d" xml:id="A45652-001-a-0450">The</w>
     <w lemma="sole" pos="j" xml:id="A45652-001-a-0460">sole</w>
     <w lemma="motive" pos="n1" xml:id="A45652-001-a-0470">Motive</w>
     <w lemma="be" pos="vvz" xml:id="A45652-001-a-0480">is</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-0490">the</w>
     <w lemma="justify" pos="vvg" xml:id="A45652-001-a-0500">Justifying</w>
     <w lemma="my" pos="po" xml:id="A45652-001-a-0510">my</w>
     <w lemma="self" pos="n1" rend="hi" xml:id="A45652-001-a-0520">Self</w>
     <w lemma="and" pos="cc" xml:id="A45652-001-a-0530">and</w>
     <w lemma="organ" pos="n1" rend="hi" xml:id="A45652-001-a-0540">Organ</w>
     <w lemma="in" pos="acp" xml:id="A45652-001-a-0550">in</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-0560">the</w>
     <w lemma="temple-church" pos="n1" rend="hi" xml:id="A45652-001-a-0570">Temple-Church</w>
     <pc xml:id="A45652-001-a-0580">,</pc>
     <w lemma="against" pos="acp" xml:id="A45652-001-a-0590">against</w>
     <w lemma="his" pos="po" xml:id="A45652-001-a-0600">his</w>
     <w lemma="scandalous" pos="j" xml:id="A45652-001-a-0610">Scandalous</w>
     <w lemma="aspersion" pos="n2" xml:id="A45652-001-a-0620">aspersions</w>
     <pc xml:id="A45652-001-a-0630">;</pc>
     <w lemma="and" pos="cc" xml:id="A45652-001-a-0640">and</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-0650">of</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-0660">the</w>
     <w lemma="make" pos="vvg" xml:id="A45652-001-a-0670">making</w>
     <w lemma="it" pos="pn" xml:id="A45652-001-a-0680">it</w>
     <w lemma="public" pos="j" reg="public" xml:id="A45652-001-a-0690">publick</w>
     <pc xml:id="A45652-001-a-0700">,</pc>
     <w lemma="that" pos="cs" xml:id="A45652-001-a-0710">that</w>
     <w lemma="i" pos="pns" xml:id="A45652-001-a-0720">I</w>
     <w lemma="may" pos="vmb" xml:id="A45652-001-a-0730">may</w>
     <pc join="right" xml:id="A45652-001-a-0740">(</pc>
     <w lemma="if" pos="cs" xml:id="A45652-001-a-0750">if</w>
     <w lemma="possible" pos="j" xml:id="A45652-001-a-0760">possible</w>
     <pc xml:id="A45652-001-a-0770">)</pc>
     <w lemma="provoke" pos="vvb" xml:id="A45652-001-a-0780">provoke</w>
     <w lemma="he" pos="pno" xml:id="A45652-001-a-0790">him</w>
     <w lemma="this" pos="d" xml:id="A45652-001-a-0800">this</w>
     <w lemma="way" pos="n1" xml:id="A45652-001-a-0810">way</w>
     <w lemma="to" pos="prt" xml:id="A45652-001-a-0820">to</w>
     <w lemma="answer" pos="vvi" xml:id="A45652-001-a-0830">answer</w>
     <w lemma="my" pos="po" xml:id="A45652-001-a-0840">my</w>
     <w lemma="challenge" pos="n1" xml:id="A45652-001-a-0850">Challenge</w>
     <pc xml:id="A45652-001-a-0860">,</pc>
     <w lemma="since" pos="acp" xml:id="A45652-001-a-0870">since</w>
     <w lemma="he" pos="pns" xml:id="A45652-001-a-0880">he</w>
     <w lemma="have" pos="vvz" xml:id="A45652-001-a-0890">has</w>
     <w lemma="already" pos="av" xml:id="A45652-001-a-0900">already</w>
     <w lemma="decline" pos="vvn" reg="declined" xml:id="A45652-001-a-0910">declin'd</w>
     <w lemma="it" pos="pn" xml:id="A45652-001-a-0920">it</w>
     <pc xml:id="A45652-001-a-0930">,</pc>
     <w lemma="when" pos="crq" xml:id="A45652-001-a-0940">when</w>
     <w lemma="send" pos="vvd" xml:id="A45652-001-a-0950">sent</w>
     <w lemma="he" pos="pno" xml:id="A45652-001-a-0960">him</w>
     <w lemma="private" pos="av-j" xml:id="A45652-001-a-0970">privately</w>
     <pc unit="sentence" xml:id="A45652-001-a-0980">.</pc>
     <w lemma="i" pos="pns" xml:id="A45652-001-a-0990">I</w>
     <w lemma="can" pos="vmd" xml:id="A45652-001-a-1000">could</w>
     <w lemma="have" pos="vvi" xml:id="A45652-001-a-1010">have</w>
     <w lemma="rest" pos="vvn" xml:id="A45652-001-a-1020">rested</w>
     <w lemma="content" pos="j" xml:id="A45652-001-a-1030">Content</w>
     <w lemma="under" pos="acp" xml:id="A45652-001-a-1040">under</w>
     <w lemma="my" pos="po" xml:id="A45652-001-a-1050">my</w>
     <w lemma="great" pos="j" xml:id="A45652-001-a-1060">great</w>
     <w lemma="loss" pos="n1" xml:id="A45652-001-a-1070">loss</w>
     <w lemma="with" pos="acp" xml:id="A45652-001-a-1080">with</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-1090">the</w>
     <w lemma="decision" pos="n1" xml:id="A45652-001-a-1100">Decision</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-1110">of</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-1120">the</w>
     <w lemma="two" pos="crd" xml:id="A45652-001-a-1130">two</w>
     <w lemma="house" pos="n2" xml:id="A45652-001-a-1140">Houses</w>
     <pc xml:id="A45652-001-a-1150">,</pc>
     <w lemma="and" pos="cc" xml:id="A45652-001-a-1160">and</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-1170">the</w>
     <w lemma="acceptance" pos="n1" xml:id="A45652-001-a-1180">acceptance</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-1190">of</w>
     <w lemma="mr." pos="ab" xml:id="A45652-001-a-1200">Mr.</w>
     <w lemma="smith" pos="ng1" rend="hi-apo-plain" xml:id="A45652-001-a-1210">Smith's</w>
     <w lemma="organ" pos="n1" xml:id="A45652-001-a-1230">Organ</w>
     <w lemma="by" pos="acp" xml:id="A45652-001-a-1240">by</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-1250">the</w>
     <w lemma="plurality" pos="n1" xml:id="A45652-001-a-1260">plurality</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-1270">of</w>
     <w lemma="four" pos="crd" xml:id="A45652-001-a-1280">four</w>
     <w lemma="voice" pos="n2" xml:id="A45652-001-a-1290">Voices</w>
     <pc xml:id="A45652-001-a-1300">;</pc>
     <w lemma="and" pos="cc" xml:id="A45652-001-a-1310">and</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-1320">the</w>
     <w lemma="rather" pos="avc" xml:id="A45652-001-a-1330">rather</w>
     <pc xml:id="A45652-001-a-1340">,</pc>
     <w lemma="for" pos="acp" xml:id="A45652-001-a-1350">for</w>
     <w lemma="that" pos="cs" xml:id="A45652-001-a-1360">that</w>
     <w lemma="it" pos="pn" xml:id="A45652-001-a-1370">it</w>
     <w lemma="be" pos="vvd" xml:id="A45652-001-a-1380">was</w>
     <w lemma="declare" pos="vvn" reg="declared" xml:id="A45652-001-a-1390">declar'd</w>
     <pc xml:id="A45652-001-a-1400">,</pc>
     <w lemma="at" pos="acp" xml:id="A45652-001-a-1410">at</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-1420">the</w>
     <w lemma="same" pos="d" xml:id="A45652-001-a-1430">same</w>
     <w lemma="time" pos="n1" xml:id="A45652-001-a-1440">time</w>
     <pc xml:id="A45652-001-a-1450">,</pc>
     <w lemma="that" pos="cs" xml:id="A45652-001-a-1460">that</w>
     <w lemma="notwithstanding" pos="acp" xml:id="A45652-001-a-1470">notwithstanding</w>
     <w lemma="that" pos="d" xml:id="A45652-001-a-1480">that</w>
     <w lemma="determination" pos="n1" xml:id="A45652-001-a-1490">Determination</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-1500">of</w>
     <w lemma="they" pos="png" xml:id="A45652-001-a-1510">theirs</w>
     <pc xml:id="A45652-001-a-1520">,</pc>
     <w lemma="i" pos="png" xml:id="A45652-001-a-1530">mine</w>
     <w lemma="may" pos="vmd" xml:id="A45652-001-a-1540">might</w>
     <w lemma="be" pos="vvi" xml:id="A45652-001-a-1550">be</w>
     <w lemma="as" pos="acp" xml:id="A45652-001-a-1560">as</w>
     <w lemma="good" pos="j" xml:id="A45652-001-a-1570">good</w>
     <w lemma="or" pos="cc" xml:id="A45652-001-a-1580">or</w>
     <w lemma="a" pos="d" xml:id="A45652-001-a-1590">a</w>
     <w lemma="better" pos="jc" xml:id="A45652-001-a-1600">better</w>
     <w lemma="organ" pos="n1" xml:id="A45652-001-a-1610">Organ</w>
     <w lemma="than" pos="cs" reg="than" xml:id="A45652-001-a-1620">then</w>
     <w lemma="mr." pos="ab" xml:id="A45652-001-a-1630">Mr.</w>
     <w lemma="smith" pos="ng1" rend="hi" xml:id="A45652-001-a-1640">Smith's</w>
     <pc xml:id="A45652-001-a-1650">;</pc>
     <w lemma="and" pos="cc" xml:id="A45652-001-a-1660">And</w>
     <w lemma="indeed" pos="av" xml:id="A45652-001-a-1670">indeed</w>
     <w lemma="it" pos="pn" xml:id="A45652-001-a-1680">it</w>
     <w lemma="can" pos="vmd" xml:id="A45652-001-a-1690">could</w>
     <w lemma="not" pos="xx" xml:id="A45652-001-a-1700">not</w>
     <w lemma="be" pos="vvi" xml:id="A45652-001-a-1710">be</w>
     <w lemma="expect" pos="vvn" xml:id="A45652-001-a-1720">expected</w>
     <pc xml:id="A45652-001-a-1730">,</pc>
     <w lemma="that" pos="cs" xml:id="A45652-001-a-1740">that</w>
     <w lemma="those" pos="d" xml:id="A45652-001-a-1750">those</w>
     <w lemma="gentleman" pos="n2" xml:id="A45652-001-a-1760">Gentlemen</w>
     <w lemma="can" pos="vmd" xml:id="A45652-001-a-1770">could</w>
     <w lemma="be" pos="vvi" xml:id="A45652-001-a-1780">be</w>
     <w lemma="such" pos="d" xml:id="A45652-001-a-1790">such</w>
     <w lemma="critic" pos="n2" reg="Critics" xml:id="A45652-001-a-1800">Criticks</w>
     <w lemma="in" pos="acp" xml:id="A45652-001-a-1810">in</w>
     <w lemma="music" pos="n1" reg="Music" xml:id="A45652-001-a-1820">Musick</w>
     <w lemma="as" pos="acp" xml:id="A45652-001-a-1830">as</w>
     <w lemma="to" pos="prt" xml:id="A45652-001-a-1840">to</w>
     <w lemma="be" pos="vvi" xml:id="A45652-001-a-1850">be</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-1860">the</w>
     <w lemma="proper" pos="j" xml:id="A45652-001-a-1870">proper</w>
     <w lemma="judge" pos="n2" xml:id="A45652-001-a-1880">Judges</w>
     <w lemma="which" pos="crq" xml:id="A45652-001-a-1890">which</w>
     <w lemma="be" pos="vvd" xml:id="A45652-001-a-1900">were</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-1910">the</w>
     <w lemma="better" pos="jc" xml:id="A45652-001-a-1920">better</w>
     <w lemma="instrument" pos="n1" xml:id="A45652-001-a-1930">Instrument</w>
     <pc unit="sentence" xml:id="A45652-001-a-1940">.</pc>
     <w lemma="but" pos="acp" xml:id="A45652-001-a-1950">But</w>
     <w lemma="when" pos="crq" xml:id="A45652-001-a-1960">when</w>
     <w lemma="my" pos="po" xml:id="A45652-001-a-1970">my</w>
     <w lemma="reputation" pos="n1" xml:id="A45652-001-a-1980">Reputation</w>
     <w lemma="in" pos="acp" xml:id="A45652-001-a-1990">in</w>
     <w lemma="my" pos="po" xml:id="A45652-001-a-2000">my</w>
     <w lemma="profession" pos="n1" xml:id="A45652-001-a-2010">Profession</w>
     <w lemma="be" pos="vvz" xml:id="A45652-001-a-2020">is</w>
     <w lemma="call" pos="vvn" reg="called" xml:id="A45652-001-a-2030">call'd</w>
     <w lemma="in" pos="acp" xml:id="A45652-001-a-2040">in</w>
     <w lemma="question" pos="n1" xml:id="A45652-001-a-2050">Question</w>
     <pc xml:id="A45652-001-a-2060">,</pc>
     <w lemma="and" pos="cc" xml:id="A45652-001-a-2070">and</w>
     <w lemma="his" pos="po" xml:id="A45652-001-a-2080">his</w>
     <w lemma="detraction" pos="n1" xml:id="A45652-001-a-2090">detraction</w>
     <w lemma="may" pos="vmd" xml:id="A45652-001-a-2100">might</w>
     <w lemma="be" pos="vvi" xml:id="A45652-001-a-2110">be</w>
     <w lemma="injurious" pos="j" xml:id="A45652-001-a-2120">injurious</w>
     <w lemma="to" pos="acp" xml:id="A45652-001-a-2130">to</w>
     <w lemma="i" pos="pno" xml:id="A45652-001-a-2140">me</w>
     <w lemma="in" pos="acp" xml:id="A45652-001-a-2150">in</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-2160">the</w>
     <w lemma="employment" pos="n1" xml:id="A45652-001-a-2170">Employment</w>
     <w lemma="i" pos="pns" xml:id="A45652-001-a-2180">I</w>
     <w lemma="have" pos="vvb" xml:id="A45652-001-a-2190">have</w>
     <w lemma="under" pos="acp" xml:id="A45652-001-a-2200">under</w>
     <w lemma="his" pos="po" xml:id="A45652-001-a-2210">his</w>
     <w lemma="sacred" pos="j" xml:id="A45652-001-a-2220">Sacred</w>
     <w lemma="majesty" pos="n1" xml:id="A45652-001-a-2230">Majesty</w>
     <pc xml:id="A45652-001-a-2240">,</pc>
     <w lemma="and" pos="cc" xml:id="A45652-001-a-2250">and</w>
     <w lemma="otherwhere" pos="av" xml:id="A45652-001-a-2260">otherwhere</w>
     <pc xml:id="A45652-001-a-2270">;</pc>
     <w lemma="i" pos="pns" xml:id="A45652-001-a-2280">I</w>
     <w lemma="hope" pos="vvb" xml:id="A45652-001-a-2290">hope</w>
     <w join="right" lemma="i" pos="pns" xml:id="A45652-001-a-2300">I</w>
     <w join="left" lemma="be" pos="vvm" xml:id="A45652-001-a-2301">'m</w>
     <w lemma="excusable" pos="j" xml:id="A45652-001-a-2310">Excusable</w>
     <w lemma="in" pos="acp" xml:id="A45652-001-a-2320">in</w>
     <w lemma="take" pos="vvg" xml:id="A45652-001-a-2330">taking</w>
     <w lemma="this" pos="d" xml:id="A45652-001-a-2340">this</w>
     <w lemma="method" pos="n1" xml:id="A45652-001-a-2350">method</w>
     <w lemma="for" pos="acp" xml:id="A45652-001-a-2360">for</w>
     <w lemma="my" pos="po" xml:id="A45652-001-a-2370">my</w>
     <w lemma="vindication" pos="n1" xml:id="A45652-001-a-2380">Vindication</w>
     <pc xml:id="A45652-001-a-2390">:</pc>
     <w lemma="all" pos="d" xml:id="A45652-001-a-2400">All</w>
     <w lemma="my" pos="po" xml:id="A45652-001-a-2410">my</w>
     <w lemma="fear" pos="n1" xml:id="A45652-001-a-2420">fear</w>
     <w lemma="be" pos="vvz" xml:id="A45652-001-a-2430">is</w>
     <pc xml:id="A45652-001-a-2440">,</pc>
     <w lemma="that" pos="cs" xml:id="A45652-001-a-2450">that</w>
     <w lemma="mr." pos="ab" xml:id="A45652-001-a-2460">Mr.</w>
     <w lemma="Smith" pos="nn1" rend="hi" xml:id="A45652-001-a-2470">Smith</w>
     <pc xml:id="A45652-001-a-2480">,</pc>
     <w lemma="who" pos="crq" xml:id="A45652-001-a-2490">who</w>
     <pc join="right" xml:id="A45652-001-a-2500">(</pc>
     <w lemma="during" pos="acp" xml:id="A45652-001-a-2510">during</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-2520">the</w>
     <w lemma="whole" pos="j" xml:id="A45652-001-a-2530">whole</w>
     <w lemma="contest" pos="vvi" xml:id="A45652-001-a-2540">Contest</w>
     <w lemma="about" pos="acp" xml:id="A45652-001-a-2550">about</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-2560">the</w>
     <w lemma="organ" pos="n2" xml:id="A45652-001-a-2570">Organs</w>
     <pc xml:id="A45652-001-a-2580">)</pc>
     <w lemma="can" pos="vmd" xml:id="A45652-001-a-2590">could</w>
     <w lemma="never" pos="avx" xml:id="A45652-001-a-2600">never</w>
     <w lemma="be" pos="vvi" xml:id="A45652-001-a-2610">be</w>
     <w lemma="bring" pos="vvn" xml:id="A45652-001-a-2620">brought</w>
     <w lemma="to" pos="acp" xml:id="A45652-001-a-2630">to</w>
     <w lemma="a" pos="d" xml:id="A45652-001-a-2640">a</w>
     <w lemma="trial" pos="n1" reg="trial" xml:id="A45652-001-a-2650">tryall</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-2660">of</w>
     <w lemma="they" pos="pno" xml:id="A45652-001-a-2670">them</w>
     <w lemma="before" pos="acp" xml:id="A45652-001-a-2680">before</w>
     <w lemma="master" pos="n2" xml:id="A45652-001-a-2690">Masters</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-2700">of</w>
     <w lemma="music" pos="n1" reg="Music" xml:id="A45652-001-a-2710">Musick</w>
     <pc xml:id="A45652-001-a-2720">,</pc>
     <w lemma="though" pos="cs" xml:id="A45652-001-a-2730">though</w>
     <w lemma="several" pos="j" xml:id="A45652-001-a-2740">several</w>
     <w lemma="time" pos="n2" xml:id="A45652-001-a-2750">times</w>
     <w lemma="there" pos="av" xml:id="A45652-001-a-2760">there</w>
     <w lemma="be" pos="vvd" xml:id="A45652-001-a-2770">were</w>
     <w lemma="appointment" pos="n2" xml:id="A45652-001-a-2780">appointments</w>
     <w lemma="make" pos="vvn" xml:id="A45652-001-a-2790">made</w>
     <w lemma="for" pos="acp" xml:id="A45652-001-a-2800">for</w>
     <w lemma="that" pos="d" xml:id="A45652-001-a-2810">that</w>
     <w lemma="purpose" pos="n1" xml:id="A45652-001-a-2820">purpose</w>
     <pc xml:id="A45652-001-a-2830">,</pc>
     <w lemma="will" pos="vmb" xml:id="A45652-001-a-2840">will</w>
     <w lemma="decline" pos="vvi" xml:id="A45652-001-a-2850">decline</w>
     <w lemma="this" pos="d" xml:id="A45652-001-a-2860">this</w>
     <w lemma="challenge" pos="n1" xml:id="A45652-001-a-2870">Challenge</w>
     <pc xml:id="A45652-001-a-2880">,</pc>
     <w lemma="notwithstanding" pos="acp" xml:id="A45652-001-a-2890">notwithstanding</w>
     <w join="right" lemma="it" pos="pn" xml:id="A45652-001-a-2900">it</w>
     <w join="left" lemma="be" pos="vvz" xml:id="A45652-001-a-2901">'s</w>
     <w lemma="propose" pos="vvn" reg="proposed" xml:id="A45652-001-a-2910">propos'd</w>
     <w lemma="so" pos="av" xml:id="A45652-001-a-2920">so</w>
     <w lemma="much" pos="av-d" xml:id="A45652-001-a-2930">much</w>
     <w lemma="to" pos="acp" xml:id="A45652-001-a-2940">to</w>
     <w lemma="his" pos="po" xml:id="A45652-001-a-2950">his</w>
     <w lemma="advantage" pos="n1" xml:id="A45652-001-a-2960">Advantage</w>
     <pc xml:id="A45652-001-a-2970">;</pc>
     <w lemma="yet" pos="av" xml:id="A45652-001-a-2980">Yet</w>
     <w lemma="if" pos="cs" xml:id="A45652-001-a-2990">if</w>
     <w lemma="he" pos="pns" xml:id="A45652-001-a-3000">he</w>
     <w lemma="be" pos="vvb" xml:id="A45652-001-a-3010">be</w>
     <w lemma="that" pos="d" xml:id="A45652-001-a-3020">that</w>
     <w lemma="man" pos="n1" xml:id="A45652-001-a-3030">man</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-3040">of</w>
     <w lemma="art" pos="n1" xml:id="A45652-001-a-3050">Art</w>
     <w lemma="he" pos="pns" xml:id="A45652-001-a-3060">he</w>
     <w lemma="pretend" pos="vvz" xml:id="A45652-001-a-3070">pretends</w>
     <pc xml:id="A45652-001-a-3080">,</pc>
     <w lemma="and" pos="cc" xml:id="A45652-001-a-3090">and</w>
     <w lemma="value" pos="vvz" xml:id="A45652-001-a-3100">Values</w>
     <w lemma="his" pos="po" xml:id="A45652-001-a-3110">his</w>
     <w lemma="reputation" pos="n1" xml:id="A45652-001-a-3120">Reputation</w>
     <w lemma="or" pos="cc" xml:id="A45652-001-a-3130">or</w>
     <w lemma="profit" pos="n1" xml:id="A45652-001-a-3140">Profit</w>
     <pc xml:id="A45652-001-a-3150">,</pc>
     <w lemma="he" pos="pns" xml:id="A45652-001-a-3160">he</w>
     <w lemma="will" pos="vmb" xml:id="A45652-001-a-3170">will</w>
     <w lemma="accept" pos="vvi" xml:id="A45652-001-a-3180">accept</w>
     <w lemma="it" pos="pn" xml:id="A45652-001-a-3190">it</w>
     <pc unit="sentence" xml:id="A45652-001-a-3200">.</pc>
    </p>
    <p xml:id="A45652-e130">
     <w lemma="now" pos="av" xml:id="A45652-001-a-3210">Now</w>
     <w lemma="my" pos="po" xml:id="A45652-001-a-3220">my</w>
     <w lemma="challenge" pos="n1" xml:id="A45652-001-a-3230">Challenge</w>
     <w lemma="to" pos="acp" xml:id="A45652-001-a-3240">to</w>
     <w lemma="he" pos="pno" xml:id="A45652-001-a-3250">him</w>
     <w lemma="be" pos="vvz" xml:id="A45652-001-a-3260">is</w>
     <w lemma="this" pos="d" xml:id="A45652-001-a-3270">this</w>
     <pc unit="sentence" xml:id="A45652-001-a-3280">.</pc>
     <lb xml:id="A45652-e140"/>
     <w lemma="that" pos="cs" xml:id="A45652-001-a-3290">That</w>
     <w lemma="both" pos="d" xml:id="A45652-001-a-3300">both</w>
     <w lemma="organ" pos="n2" xml:id="A45652-001-a-3310">Organs</w>
     <w lemma="stand" pos="vvg" xml:id="A45652-001-a-3320">standing</w>
     <w lemma="as" pos="acp" xml:id="A45652-001-a-3330">as</w>
     <w lemma="now" pos="av" xml:id="A45652-001-a-3340">now</w>
     <w lemma="they" pos="pns" xml:id="A45652-001-a-3350">they</w>
     <w lemma="do" pos="vvb" xml:id="A45652-001-a-3360">do</w>
     <pc xml:id="A45652-001-a-3370">,</pc>
     <w lemma="upon" pos="acp" xml:id="A45652-001-a-3380">upon</w>
     <w lemma="equal" pos="j" xml:id="A45652-001-a-3390">equal</w>
     <w lemma="advantage" pos="n1" xml:id="A45652-001-a-3400">advantage</w>
     <pc xml:id="A45652-001-a-3410">,</pc>
     <w lemma="to" pos="prt" xml:id="A45652-001-a-3420">to</w>
     <w lemma="play" pos="vvi" xml:id="A45652-001-a-3430">play</w>
     <w lemma="every" pos="d" xml:id="A45652-001-a-3440">every</w>
     <w lemma="individual" pos="j" xml:id="A45652-001-a-3450">Individual</w>
     <w lemma="stop" pos="n1" xml:id="A45652-001-a-3460">Stop</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-3470">of</w>
     <w lemma="my" pos="po" xml:id="A45652-001-a-3480">my</w>
     <w lemma="organ" pos="n1" rend="hi" xml:id="A45652-001-a-3490">Organ</w>
     <w lemma="against" pos="acp" xml:id="A45652-001-a-3500">against</w>
     <w lemma="every" pos="d" xml:id="A45652-001-a-3510">every</w>
     <w lemma="stop" pos="n1" xml:id="A45652-001-a-3520">Stop</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-3530">of</w>
     <w lemma="he" pos="png" xml:id="A45652-001-a-3540">his</w>
     <pc xml:id="A45652-001-a-3550">,</pc>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-3560">of</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-3570">the</w>
     <w lemma="like" pos="j" xml:id="A45652-001-a-3580">like</w>
     <w lemma="denomination" pos="n1" xml:id="A45652-001-a-3590">Denomination</w>
     <pc xml:id="A45652-001-a-3600">,</pc>
     <w lemma="for" pos="acp" xml:id="A45652-001-a-3610">for</w>
     <w lemma="ten" pos="crd" xml:id="A45652-001-a-3620">ten</w>
     <w lemma="pound" pos="n2" xml:id="A45652-001-a-3630">pounds</w>
     <w lemma="each" pos="d" xml:id="A45652-001-a-3640">each</w>
     <w lemma="stop" pos="n1" xml:id="A45652-001-a-3650">stop</w>
     <pc xml:id="A45652-001-a-3660">;</pc>
     <w lemma="and" pos="cc" xml:id="A45652-001-a-3670">and</w>
     <w lemma="that" pos="cs" xml:id="A45652-001-a-3680">that</w>
     <w lemma="if" pos="cs" xml:id="A45652-001-a-3690">if</w>
     <w lemma="he" pos="pns" xml:id="A45652-001-a-3700">he</w>
     <w lemma="value" pos="vvz" xml:id="A45652-001-a-3710">Values</w>
     <w lemma="one" pos="crd" xml:id="A45652-001-a-3720">one</w>
     <w lemma="or" pos="cc" xml:id="A45652-001-a-3730">or</w>
     <w lemma="more" pos="avc-d" xml:id="A45652-001-a-3740">more</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-3750">of</w>
     <w lemma="his" pos="po" xml:id="A45652-001-a-3760">his</w>
     <w lemma="stop" pos="n2" xml:id="A45652-001-a-3770">stops</w>
     <w lemma="more" pos="avc-d" xml:id="A45652-001-a-3780">more</w>
     <w lemma="than" pos="cs" reg="than" xml:id="A45652-001-a-3790">then</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-3800">the</w>
     <w lemma="rest" pos="n1" xml:id="A45652-001-a-3810">rest</w>
     <pc xml:id="A45652-001-a-3820">,</pc>
     <w lemma="to" pos="prt" xml:id="A45652-001-a-3830">to</w>
     <w lemma="double" pos="j" xml:id="A45652-001-a-3840">double</w>
     <w lemma="upon" pos="acp" xml:id="A45652-001-a-3850">upon</w>
     <w lemma="that" pos="d" xml:id="A45652-001-a-3860">that</w>
     <w lemma="or" pos="cc" xml:id="A45652-001-a-3870">or</w>
     <w lemma="they" pos="pno" xml:id="A45652-001-a-3880">them</w>
     <pc unit="sentence" xml:id="A45652-001-a-3890">.</pc>
     <w lemma="i" pos="pns" xml:id="A45652-001-a-3900">I</w>
     <w lemma="propose" pos="vvb" xml:id="A45652-001-a-3910">propose</w>
     <w lemma="that" pos="cs" xml:id="A45652-001-a-3920">that</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-3930">the</w>
     <w lemma="judge" pos="n2" xml:id="A45652-001-a-3940">Judges</w>
     <w lemma="be" pos="vvb" xml:id="A45652-001-a-3950">be</w>
     <w lemma="skilful" pos="j" reg="Skilful" xml:id="A45652-001-a-3960">Skillful</w>
     <w lemma="organist" pos="n2" rend="hi" xml:id="A45652-001-a-3970">Organists</w>
     <pc xml:id="A45652-001-a-3980">,</pc>
     <w lemma="who" pos="crq" xml:id="A45652-001-a-3990">who</w>
     <w lemma="be" pos="vvg" xml:id="A45652-001-a-4000">being</w>
     <w lemma="by" pos="acp" xml:id="A45652-001-a-4010">by</w>
     <w lemma="we" pos="pno" xml:id="A45652-001-a-4020">us</w>
     <w lemma="equal" pos="av-j" xml:id="A45652-001-a-4030">equally</w>
     <w lemma="choose" pos="vvn" xml:id="A45652-001-a-4040">Chosen</w>
     <pc xml:id="A45652-001-a-4050">,</pc>
     <w lemma="may" pos="vmb" xml:id="A45652-001-a-4060">may</w>
     <pc xml:id="A45652-001-a-4070">,</pc>
     <w lemma="if" pos="cs" xml:id="A45652-001-a-4080">if</w>
     <w lemma="there" pos="av" xml:id="A45652-001-a-4090">there</w>
     <w lemma="be" pos="vvi" xml:id="A45652-001-a-4100">be</w>
     <w lemma="occasion" pos="n1" xml:id="A45652-001-a-4110">occasion</w>
     <pc xml:id="A45652-001-a-4120">,</pc>
     <w lemma="elect" pos="vvb" xml:id="A45652-001-a-4130">elect</w>
     <w lemma="a" pos="d" xml:id="A45652-001-a-4140">an</w>
     <w lemma="umpire" pos="n1" xml:id="A45652-001-a-4150">Umpire</w>
     <pc unit="sentence" xml:id="A45652-001-a-4160">.</pc>
     <w lemma="that" pos="cs" xml:id="A45652-001-a-4170">That</w>
     <w lemma="they" pos="pns" xml:id="A45652-001-a-4180">they</w>
     <w lemma="be" pos="vvb" xml:id="A45652-001-a-4190">be</w>
     <w lemma="request" pos="vvn" xml:id="A45652-001-a-4200">requested</w>
     <w lemma="to" pos="prt" xml:id="A45652-001-a-4210">to</w>
     <w lemma="take" pos="vvi" xml:id="A45652-001-a-4220">take</w>
     <w lemma="a" pos="d" xml:id="A45652-001-a-4230">a</w>
     <w lemma="voluntary" pos="j" xml:id="A45652-001-a-4240">Voluntary</w>
     <w lemma="oath" pos="n1" xml:id="A45652-001-a-4250">Oath</w>
     <pc xml:id="A45652-001-a-4260">,</pc>
     <w lemma="to" pos="prt" xml:id="A45652-001-a-4270">to</w>
     <w lemma="declare" pos="vvi" xml:id="A45652-001-a-4280">declare</w>
     <w lemma="their" pos="po" xml:id="A45652-001-a-4290">their</w>
     <w lemma="opinion" pos="n2" xml:id="A45652-001-a-4300">Opinions</w>
     <pc xml:id="A45652-001-a-4310">,</pc>
     <w lemma="without" pos="acp" xml:id="A45652-001-a-4320">without</w>
     <w lemma="favour" pos="n1" xml:id="A45652-001-a-4330">favour</w>
     <w lemma="or" pos="cc" xml:id="A45652-001-a-4340">or</w>
     <w lemma="affection" pos="n1" xml:id="A45652-001-a-4350">affection</w>
     <pc xml:id="A45652-001-a-4360">,</pc>
     <w lemma="according" pos="j" xml:id="A45652-001-a-4370">according</w>
     <w lemma="to" pos="acp" xml:id="A45652-001-a-4380">to</w>
     <w lemma="the" pos="d" xml:id="A45652-001-a-4390">the</w>
     <w lemma="best" pos="js" xml:id="A45652-001-a-4400">best</w>
     <w lemma="of" pos="acp" xml:id="A45652-001-a-4410">of</w>
     <w lemma="their" pos="po" xml:id="A45652-001-a-4420">their</w>
     <w lemma="skill" pos="n1" xml:id="A45652-001-a-4430">Skill</w>
     <pc xml:id="A45652-001-a-4440">;</pc>
     <w lemma="and" pos="cc" xml:id="A45652-001-a-4450">and</w>
     <w lemma="we" pos="pns" xml:id="A45652-001-a-4460">we</w>
     <w lemma="be" pos="vvb" xml:id="A45652-001-a-4470">be</w>
     <w lemma="oblige" pos="vvn" xml:id="A45652-001-a-4480">obliged</w>
     <w lemma="to" pos="prt" xml:id="A45652-001-a-4490">to</w>
     <w lemma="stand" pos="vvi" xml:id="A45652-001-a-4500">stand</w>
     <w lemma="to" pos="acp" xml:id="A45652-001-a-4510">to</w>
     <w lemma="their" pos="po" xml:id="A45652-001-a-4520">their</w>
     <w lemma="determination" pos="n1" xml:id="A45652-001-a-4530">Determination</w>
     <pc unit="sentence" xml:id="A45652-001-a-4540">.</pc>
     <w lemma="last" pos="ord" xml:id="A45652-001-a-4550">Lastly</w>
     <pc xml:id="A45652-001-a-4560">,</pc>
     <w lemma="that" pos="cs" xml:id="A45652-001-a-4570">that</w>
     <w lemma="we" pos="pns" xml:id="A45652-001-a-4580">we</w>
     <w lemma="article" pos="n1" xml:id="A45652-001-a-4590">Article</w>
     <w lemma="as" pos="acp" xml:id="A45652-001-a-4600">as</w>
     <w lemma="far" pos="av-j" xml:id="A45652-001-a-4610">far</w>
     <w lemma="as" pos="acp" xml:id="A45652-001-a-4620">as</w>
     <w lemma="shall" pos="vmb" xml:id="A45652-001-a-4630">shall</w>
     <w lemma="be" pos="vvi" xml:id="A45652-001-a-4640">be</w>
     <w lemma="necessary" pos="j" xml:id="A45652-001-a-4650">Necessary</w>
     <w lemma="to" pos="prt" xml:id="A45652-001-a-4660">to</w>
     <w lemma="render" pos="vvi" xml:id="A45652-001-a-4670">render</w>
     <w lemma="this" pos="d" xml:id="A45652-001-a-4680">this</w>
     <w lemma="proposition" pos="n1" xml:id="A45652-001-a-4690">Proposition</w>
     <w lemma="complete" pos="j" reg="Complete" xml:id="A45652-001-a-4700">Compleat</w>
     <pc unit="sentence" xml:id="A45652-001-a-4710">.</pc>
    </p>
   </div>
  </body>
 </text>
</TEI>
